.PHONY=push
push:
	git push -v origin main

.PHONY=update
update:
	git checkout main && git pull -v && git rebase

.PHONY=status stat
status stat:
	@echo
	git branch -a -v
	@echo
	git status
	@echo
