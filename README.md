# techrefs

A collection of technical references and comments.

* [IETF RFC considerations](./ietf_rfc_considerations.md)
* [DNS considerations](./dns_considerations.md)
* [other](./other.md)
