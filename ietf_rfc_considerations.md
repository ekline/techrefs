# RFC Considerations

This page collects several references that might prove handy when writing an Internet Draft (I-D).

- some fun [history](https://spectrum.ieee.org/tech-history/cyberspace/todays-internet-still-relies-on-an-arpanetera-protocol-the-request-for-comments) of RFCs


## IETF principles
- [RFC 3935](https://rfc-editor.org/rfc/rfc3935)
  A Mission Statement for the IETF
- [RFC 8890](https://rfc-editor.org/rfc/rfc8890)
  The Internet is for End Users

## Internet architecture
- [RFC 1925](https://rfc-editor.org/rfc/rfc1925)
  The Twelve Networking Truths _(April 1st)_
- [RFC 1958](https://rfc-editor.org/rfc/rfc1958)
  Architectural Principles of the Internet
- [RFC 3439](https://rfc-editor.org/rfc/rfc3439)
  Some Internet Architectural Guidelines and Philosophy

## document category

- [BCP 9 (RFC 6410)](https://rfc-editor.org/rfc/rfc6410) standards track maturity levels
- [BCP 9 (RFC 2026)](https://www.rfc-editor.org/rfc/rfc2026#section-5) BCPs
- [BCP 9 (RFC 2026)](https://www.rfc-editor.org/rfc/rfc2026#section-4.2) non-standards track maturity levels

## protocol design

- [RFC 5128](https://rfc-editor.org/rfc/rfc5218)
  What Makes for a Successful Protocol?
- [RFC 6709](https://rfc-editor.org/rfc/rfc6709)
  Design Considerations for Protocol Extensions
- [RFC 8085](https://rfc-editor.org/rfc/rfc8085)
  UDP Usage Guidelines
- [RFC 8170](https://rfc-editor.org/rfc/rfc8170)
  Planning for Protocol Adoption and Subsequent Transitions
- [RFC 8546](https://rfc-editor.org/rfc/rfc8546)
  The Wire Image of a Network Protocol
- [RFC 8558](https://rfc-editor.org/rfc/rfc8558)
  Transport Protocol Path Signals

## security &amp; privacy

- [RFC 3704](https://rfc-editor.org/rfc/rfc3704)
  BCP38 (Ingress Filtering) and multihomed networks
- [RFC 3552](https://rfc-editor.org/rfc/rfc3552)
  Guidelines for Writing RFC Text on Security Considerations
- [RFC 4086](https://rfc-editor.org/rfc/rfc4086)
  Randomness Requirements for Security
- [RFC 4107](https://rfc-editor.org/rfc/rfc4107)
  Guidelines for Cryptographic Key Management
- [RFC 4732](https://rfc-editor.org/rfc/rfc4732)
  Internet Denial-of-Service Considerations
- [RFC 4949](https://rfc-editor.org/rfc/rfc4949)
  Internet Security Glossary, Version 2
- [RFC 5082](https://rfc-editor.org/rfc/rfc5082)
  The Generalized TTL Security Mechanism (GTSM)
- [RFC 6056](https://rfc-editor.org/rfc/rfc6056)
  Recommendations for Transport-Protocol Port Randomization
- [RFC 6071](https://rfc-editor.org/rfc/rfc6071)
  IP Security (IPsec) and Internet Key Exchange (IKE) Document Roadmap
- [RFC 6973](https://rfc-editor.org/rfc/rfc6973)
  Privacy Considerations for Internet Protocols
- [RFC 7258](https://rfc-editor.org/rfc/rfc7258)
  Pervasive Monitoring Is an Attack
- [RFC 7525](https://rfc-editor.org/rfc/rfc7525)
  Recommendations for Secure Use of Transport Layer Security (TLS) and Datagram Transport Layer Security (DTLS)
- [RFC 7696 (BCP 201)](https://rfc-editor.org/rfc/rfc7696)
  Guidelines for Cryptographic Algorithm Agility and Selecting Mandatory-to-Implement Algorithms
- [RFC 8937](https://www.rfc-editor.org/rfc/rfc8937)
  Randomness Improvements for Security Protocols

## Diffserv &amp; ECN

- [RFC 2474](https://www.rfc-editor.org/rfc/rfc2474)
  Definition of the Differentiated Services Field (DS Field) in the IPv4 and IPv6 Headers
- [RFC 2475](https://www.rfc-editor.org/rfc/rfc2475)
  An Architecture for Differentiated Services
- [RFC 2983](https://www.rfc-editor.org/rfc/rfc2983)
  Differentiated Services and Tunnels
- [RFC 3168](https://www.rfc-editor.org/rfc/rfc3168)
  The Addition of Explicit Congestion Notification (ECN) to IP
- [RFC 3260](https://www.rfc-editor.org/rfc/rfc3260)
  New Terminology and Clarifications for Diffserv
- [RFC 6040](https://www.rfc-editor.org/rfc/rfc6040)
  Tunnelling of Explicit Congestion Notification
- [RFC 8087](https://www.rfc-editor.org/rfc/rfc8087)
  The Benefits of Using Explicit Congestion Notification (ECN)

## MPLS

- [RFC 3031](https://www.rfc-editor.org/rfc/rfc3031)
  Multiprotocol Label Switching Architecture
- [RFC 3032](https://www.rfc-editor.org/rfc/rfc3032)
  MPLS Label Stack Encoding
- [RFC 3270](https://www.rfc-editor.org/rfc/rfc3270)
  Multi-Protocol Label Switching (MPLS) Support of Differentiated Services
- [RFC 5129](https://www.rfc-editor.org/rfc/rfc5129)
  Explicit Congestion Marking in MPLS

## Operations (ops) &amp; Management

- [RFC 5706](https://rfc-editor.org/rfc/rfc5706)
  Guidelines for Considering Operations and Management of New Protocols and Protocol Extensions

## other topics

- [RFC 2804](https://rfc-editor.org/rfc/rfc2804)
  IETF Policy on Wiretapping
- [RFC 4084](https://rfc-editor.org/rfc/rfc4084)
  Terminology for Describing Internet Connectivity
- [RFC 7258](https://rfc-editor.org/rfc/rfc7258)
  Pervasive Monitoring Is an Attack

## IANA considerations

- [protocol registration procedures](
  https://www.iana.org/help/protocol-registration)
- [RFC 8128](https://rfc-editor.org/rfc/rfc8126)
  Guidelines for Writing an IANA Considerations Section in RFCs
- [RFC 7120](https://rfc-editor.org/rfc/rfc7120)
  Early IANA Allocation of Standards Track Code Points
- [RFC 6838](https://rfc-editor.org/rfc/rfc6838)
  Media Type Specifications and Registration Procedures
- [RFC 8726](https://rfc-editor.org/rfc/rfc8726)
  How Requests for IANA Action Will Be Handled on the Independent Stream

### timezone database

- [tz db/data/code](https://www.iana.org/time-zones)
- [RFC 6557](https://www.rfc-editor.org/rfc/rfc6557)
  Procedures for Maintaining the Time Zone Database

## general guidance

- [style guide](
  https://rfc-editor.org/styleguide/)
- [list of abbreviations](
  https://rfc-editor.org/materials/abbrev.expansion.txt)
- [IESG statement: on "surprise authorship"](
  https://ietf.org/about/groups/iesg/statements/internet-draft-authorship/)
- [IESG statement: Normative and Informative References](
  https://ietf.org/about/groups/iesg/statements/normative-informative-references/)
- on inclusive language:
  - [IESG statement: Oppressive or Exclusionary Language](
    https://www.ietf.org/about/groups/iesg/statements/statement-on-oppressive-exclusionary-language/)
  - [IAB Statement on Inclusive Language in IAB Stream Documents](
    https://www.iab.org/documents/correspondence-reports-documents/2021-2/iab-statement-on-inclusive-language-in-iab-stream-documents/)
  - [Guidance for NIST Staff on Using Inclusive Language in Documentary Standards](
    https://nvlpubs.nist.gov/nistpubs/ir/2021/NIST.IR.8366.pdf)
- [RFC 4101](https://rfc-editor.org/rfc/rfc4101) Writing Protocol Models
- [RFC 7942](https://rfc-editor.org/rfc/rfc7942)
  Improving Awareness of Running Code: The Implementation Status Section
- downref registry
  - [registry](https://datatracker.ietf.org/doc/downref/)
  - [RFC 3967](https://www.rfc-editor.org/rfc/rfc3967)
    Clarifying when Standards Track Documents may Refer Normatively to Documents at a Lower Level
  - [RFC 4897](https://www.rfc-editor.org/rfc/rfc4897)
    Handling Normative References to Standards-Track Documents
  - [RFC 8067](https://www.rfc-editor.org/rfc/rfc8067)
    Updating When Standards Track Documents May Refer Normatively to Documents at a Lower Level
