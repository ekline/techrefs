# DNS Considerations

This page collects miscellaneous references that pertain to DNS issues.

## Relevant Organizations
- [DNS OARC](https://dns-oarc.net/)
  DNS Operations, Analysis, and Research Center
  - [chat](https://chat.dns-oarc.net/)
- [ICANN](https://icann.org/)
  Internet Corporation for Assigned Names and Numbers

## Public Suffix List (PSL)
- [ICANN OCTO-011](
  https://www.icann.org/en/system/files/files/octo-011-18may20-en.pdf)
  The Public Suffix List: A Guide for TLD Administrators
- [ICANN SAC070](
  https://www.icann.org/en/system/files/files/sac-070-en.pdf)
  SSAC Advisory on the Use of Static TLD / Suffix Lists
- [Public Suffix List Problems](https://github.com/sleevi/psl-problems)

