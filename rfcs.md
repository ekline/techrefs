# IETF RFCs

## Architecture

- [RFC 1925](https://tools.ietf.org/html/rfc1925)
  The Twelve Networking Truths (April 1st)

- [RFC 1958](https://tools.ietf.org/html/rfc1958)
  Architectural Principles of the Internet

- [RFC 3439](https://tools.ietf.org/html/rfc3439)
  Some Internet Architectural Guidelines and Philosophy

## Design

- [RFC 5128](https://tools.ietf.org/html/rfc5218)
  What Makes for a Successful Protocol?

- [RFC 6709](https://tools.ietf.org/html/rfc6709)
  Design Considerations for Protocol Extensions

- [RFC 8170](https://tools.ietf.org/html/rfc8170)
  Planning for Protocol Adoption and Subsequent Transitions

- [RFC 8546](https://tools.ietf.org/html/rfc8546)
  The Wire Image of a Network Protocol

- [RFC 8558](https://tools.ietf.org/html/rfc8558)
  Transport Protocol Path Signals

## Security

- [RFC 3704](https://tools.ietf.org/html/rfc3704)
  BCP38 (Ingress Filtering) and multihomed networks

- [RFC 5082](https://tools.ietf.org/html/rfc5082)
  The Generalized TTL Security Mechanism (GTSM)

## Process

### RFCs

- [RFC 3553](https://www.rfc-editor.org/rfc/rfc3553.html)
  An IETF URN Sub-namespace for Registered Protocol Parameters

- [RFC 7221](https://tools.ietf.org/html/rfc7221)
  Handling of Internet-Drafts by IETF Working Groups

- [RFC 8126](https://tools.ietf.org/html/rfc8126)
  Guidelines for Writing an IANA Considerations Section in RFCs

- [IESG Statement: Normative and Informative References](
  https://www.ietf.org/about/groups/iesg/statements/normative-informative-references/)

- [Guidance on Area Director Sponsoring of Documents](
  https://www.ietf.org/about/groups/iesg/statements/area-director-sponsoring-documents/)

### Other SDOs

- [RFC 4053](https://tools.ietf.org/html/rfc4053)
  Procedures for Handling Liaison Statements to and from the IETF
